package com.batch2.latihan.services;
import com.batch2.latihan.dto.DosenDto;
import com.batch2.latihan.model.Dosen;
import com.batch2.latihan.repository.DosenRepository;
import java.util.List;
import java.util.stream.Collectors;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DosenService {
    @Autowired
    private DosenRepository dosenRepository;


    public List<DosenDto> getDosenDto(){
        List<Dosen> dosens = dosenRepository.findAll();
        ModelMapper mm = new ModelMapper();

        return dosens.stream().map(x -> mm.map(x, DosenDto.class)).collect(Collectors.toList());
    }
}
