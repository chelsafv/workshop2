package com.batch2.latihan.services;

import com.batch2.latihan.dto.UserDto;
import com.batch2.latihan.model.User;
import com.batch2.latihan.repository.UserRepository;
import java.util.List;
import java.util.stream.Collectors;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

  @Autowired
  private UserRepository userRepository;


  public List<UserDto> getUserDto(){
    List<User> users = userRepository.findAll();
    ModelMapper mm = new ModelMapper();

    return users.stream().map(x -> mm.map(x, UserDto.class)).collect(Collectors.toList());
  }
}
