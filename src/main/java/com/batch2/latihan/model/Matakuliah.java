package com.batch2.latihan.model;
//tabel matakuliah
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "tbl_matakuliah")
public class Matakuliah {
    @Id
    @Column(name = "kode_mk")
    private Long kodeMk;
    private String namaMk;
    private String sks;
    private String dose;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "dosen_nip")
    private Dosen dosen;
}
