package com.batch2.latihan.model;
//tabel dosen
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "tbl_dosen")
@NoArgsConstructor
public class Dosen {
    @Id
    private Long nip;
    private String namaDosen;

    @OneToMany(mappedBy = "dosen", fetch = FetchType.LAZY)
    private List<Matakuliah> matakuliah;

    public Dosen(Long nip){
        this.nip = nip;
    }
}
