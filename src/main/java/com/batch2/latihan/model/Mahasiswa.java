package com.batch2.latihan.model;
//tabel mhs
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "tbl_mahasiswa")
public class Mahasiswa {
    @Id
    private Long nim;
    private String namaMhs;
    private String alamat;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "dosen_nip")
    @JsonBackReference
    private Dosen nip;


}
