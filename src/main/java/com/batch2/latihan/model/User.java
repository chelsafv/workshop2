package com.batch2.latihan.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "mst_user")
public class User {
    @Id
    private Long id;
    private String username;
    private String email;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "role_id")
    private Role role;

}
