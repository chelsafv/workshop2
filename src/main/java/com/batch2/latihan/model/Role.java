package com.batch2.latihan.model;

import javax.persistence.Column;
import javax.persistence.FetchType;
import jdk.jfr.Enabled;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Data
@Entity
@Table(name = "mst_role")
@NoArgsConstructor
public class Role {

    @Id
    private Long id;
    private String roleName;
    @Column(name = "permission_desc")
    private String description;

    @OneToMany(mappedBy = "role", fetch = FetchType.LAZY)
    private List<Permission> permissions;

    public Role(Long id){
        this.id = id;
    }


}
