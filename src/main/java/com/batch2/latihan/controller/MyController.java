package com.batch2.latihan.controller;

import com.batch2.latihan.dto.UserDto;
import com.batch2.latihan.model.CustomMappingModel;
import com.batch2.latihan.model.User;
import com.batch2.latihan.repository.UserRepository;
import com.batch2.latihan.services.CustomQueryDAO;
import com.batch2.latihan.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/myApi")
public class MyController {

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private UserService userService;

    @Autowired
    private CustomQueryDAO ccDao;

    @GetMapping("/hello")
    public String myFirst(){
        return "<h1 style='color:blue'> Hello World </h1>";
    }

    @GetMapping("/getUsers") //gagal
    public ResponseEntity getUser(){
        List<User> result = userRepo.findAll();
        return ResponseEntity.ok(result);
    }

    @GetMapping("/getUserDto")
    public ResponseEntity getUserDto() {
        List<UserDto> userDtos = userService.getUserDto();
        return ResponseEntity.ok(userDtos);
    }


    @PostMapping("/saveUserData")
    public ResponseEntity<User> saveUser(@RequestBody User user){
        User u = userRepo.save(user);
        return new ResponseEntity<>(u,HttpStatus.CREATED);
    }

    @GetMapping("/nativeQuery")
    public ResponseEntity<List<CustomMappingModel>> getNativeQuery(@RequestParam String userName){
        List<CustomMappingModel> list = ccDao.getCustomQueryNative(userName);
        return ResponseEntity.ok(list);
    }
    //http://localhost:8080/myApi/nativeQuery?userName=jasmine

}
