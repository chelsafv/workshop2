package com.batch2.latihan.controller;

import com.batch2.latihan.model.Role;
import com.batch2.latihan.model.User;
import com.batch2.latihan.repository.UserRepository;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@RestController
@RequestMapping("poi")
public class PoiController {

    @Autowired
    private UserRepository userRepository;

    @PostMapping("/upload")
    public ResponseEntity<List<User>> uploadFile(@RequestParam("fileExcel")MultipartFile file){
        try{
            List<User> list = new ArrayList<>();

            Workbook wb = new XSSFWorkbook(file.getInputStream());
            Sheet sheet = wb.getSheet("sheet_user");
            Iterator<Row> rowIterator = sheet.iterator();

            int rownum = 0;
            Row currentRow;
            while(rowIterator.hasNext()){
                currentRow = rowIterator.next();

                //skip Header
                if(rownum == 0){
                    rownum++;
                    continue;
                }

                User u = new User();
                u.setId(Double.valueOf(currentRow.getCell(1).getNumericCellValue()).longValue());
                u.setEmail(currentRow.getCell(2).getStringCellValue());
                u.setUsername(currentRow.getCell(3).getStringCellValue());
                u.setRole(new Role(Double.valueOf(currentRow.getCell(5).getNumericCellValue()).longValue()));
                u = userRepository.save(u);
                list.add(u);
            }
            return new ResponseEntity<>(list, HttpStatus.OK);
        }catch(IOException ioe){
            throw new RuntimeException("Gagal proses file excel: "+ioe.getMessage());
        }
    }


    @PostMapping("/uploadManual")
    public ResponseEntity<String> uploadManual(@RequestParam("fileExcel")MultipartFile file){
        try{
            List<User> list = new ArrayList<>();

            Workbook wb = new XSSFWorkbook(file.getInputStream());
            Sheet sheet = wb.getSheet("sheet_user");
            Row row = sheet.getRow(2);
            Cell cell4 = row.getCell(4);
            String x = cell4.getStringCellValue();


            return new ResponseEntity<>(x, HttpStatus.OK);
        }catch(IOException ioe){
            throw new RuntimeException("Gagal proses file excel: "+ioe.getMessage());
        }
    }
}
