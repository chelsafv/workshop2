package com.batch2.latihan.dto;
import lombok.Data;

@Data
public class MahasiswaDto {
    private Long nim;
    private String namaMhs;
    private String alamat;
}
